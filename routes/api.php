<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ConvertToJsonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('body', ConvertToJsonController::class);
Route::post('upload',[ConvertToJsonController::class,'upload']);
//Route::post('webhook', 'WebhookController@handle');
Route::get('apiTest',[ConvertToJsonController::class,'apiTest']);