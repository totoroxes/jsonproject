<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Storage;
use GuzzleHttp\Client;

class ConvertToJsonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $req) {
        if ($req->file('file') != null) {
            $result = $req->file('file');
            $data = file_get_contents($result->getRealPath());
            $array = array();
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $data) as $line){
                if(str_contains($line, "\"body\"")){
                    $linecut = str_replace("\"body\": ", "", $line);
                    $linecut = rtrim($linecut, ",");
                    array_push($array, $linecut);
                    // array_push($array, json_decode($linecut));
                }
            } 
            $bodyJson = array();
            // dd(count($array));
            if(count($array) > 0) {
                foreach($array as $index=>$item) {
 
                    $decoded_array[$index] = json_decode($array[$index]);
                    array_push($bodyJson, json_decode($decoded_array[$index]));
                    
                }
                return response()->json(['body'=>($bodyJson)],200);
            } else {
                return response()->json(['error_status' => 'true', 'error_desc' => 'Your file is not support please check your file'],400);

            }
            
        } else {
            return response()->json(['error_status' => 'true', 'error_desc' => 'File not found or check your parameter is not correct'],400);
        }
    }
    public function index()
    {

        // $path = public_path('post-ex.txt');
        // $path = base_path().'/public';
        // dd($path);
        // $files = File::files($path.'/post-ex.txt');
        // $files = Storage::get('\public\post-webhook.txt');
        // dd($files);
        // $data = $files;
        // dd($data);
        // $get_result_arr = json_encode($data, true);
        // $decode = json_decode($get_result_arr['body']);
        // // $decode2 = json_decode($decode['body']);
        // return ($decode);
        // header('Content-type: application/json');
        // echo json_encode(explode("\r\n",$data));
       
        // $js = json_decode($get_result_arr, true);
        // return $js;
        // foreach ($get_result_arr as $value) {
        //     $get_body = json_decode($value['body']);
        //     dd($get_body);
        // }

        // $get_result_arr = json_decode($data, true); //object
        // dd($get_result_arr);
        // $json = json_decode($get_result_arr['body']); //array
        // dd($temp);
        // foreach ($array as $value) {
        //     // code to be executed;
        // }

        // $file = fopen("\public\post-ex.txt","r");
        // echo fgets($file);
        // fclose($file);

        // one obj
        $files = Storage::get('\public\post-webhook.txt');
        $data = $files;
        //preg_replace('::ffff:10.4.178.61 - -','',$data);
        //dd($data);

        $bracket = 0;
        $isJSON = false;
        $json = "";
        $obj = 0;

        $outarr['output'] = array();

        Storage::delete('\public\output.txt');
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $data) as $line){
            // if(str_contains($line, "\"body\"")){
            //     $linecut = str_replace("\"body\": ", "", $line);
            //     $linecut = rtrim($linecut, ",");
            //     //dd($linecut);
            //     $json = json_decode($linecut, true);
            //     //dd($json);
            //     echo $json . PHP_EOL;
            // }

        //Check for JSON object
            if(str_contains($line, "{")){
                $bracket++;
                $isJSON = true;
            }

            if($bracket > 0)
                $json = $json . $line;
            
            if(str_contains($line, "}"))
                $bracket--;

            if($bracket == 0 && $isJSON){
                $obj++;
                $decoded = json_decode($json, true);
                //dd($decoded);

            //Count indexed array 
                echo count($decoded['json']['alerts']). "<br>";

            //Loop for indexed array
                for($i = 0; $i < count($decoded['json']['alerts']); $i++)
                    echo $decoded['json']['alerts'][$i]['labels']['alertname']. "<br>";

            //Accessing the fields (Output 1)
                $output['field1'] = $decoded['headers']['user-agent'];
                $output['field2'] = $decoded['method'];
                $output['field3'] = '';
                if(array_key_exists('status',$decoded['json']['alerts'][0])) $output['field3'] = $decoded['json']['alerts'][0]['status'];
                $output['field4'] = $decoded['json']['alerts'][0]['labels']['alertname'];
                if(isset($decoded['json']['commonAnnotations']['summary']))
                    $output['field5'] = $decoded['json']['commonAnnotations']['summary'];
                else $output['field5'] = "N/A";

                //echo $output.'<br>';
                $output_encode = json_encode($output);
                echo $output_encode ."<br>";

            //Push into indexed array (Output 2)
                array_push($outarr['output'], $output_encode);

                //fwrite($fp, $output_encode . "\r\n");

            //Output 1
                Storage::append('\public\output.txt', $output_encode . "\r\n");
                
                $json = "";
                $isJSON = false;
            }
        }
        //dd($outarr);
    //Output 2
        $outarr_encode = json_encode($outarr);
        echo $outarr_encode . "<br>";
        Storage::put('\public\output2.txt', $outarr_encode . "\r\n");
        echo $obj . "<br>";
        
    //Test accessing output
    //Output 1
        $outfile = Storage::get('\public\output.txt');
        $outdata = $outfile;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $outdata) as $line){
            $jsonout = json_decode($line, true);
            //dd($jsonout); 
            if(!empty($jsonout)){
                foreach($jsonout as $field => $val){
                    echo "$field : $val <br>";
                }
                echo "<br>";
            }
                
        }
    
    //Output 2
        $outfile = Storage::get('\public\output2.txt');
        $outdata = $outfile;
        //dd($outdata);
        $outdecode = json_decode($outdata,true);
        echo $outdecode['output'][6] . "<br>";
        //dd($outdecode);
        
        $testout = json_decode($outdecode['output'][6], true);
        echo $testout['field2'] . "<br>";
        //dd($testout);

        $testout2 = json_decode($outdecode['output'][6]);
        echo $testout2->field1 . "<br>";
        //dd($testout2);

/*
        $get_result_arr = json_decode($data, true); //object
        //dd($get_result_arr);
        $json = json_decode($get_result_arr['body']); //array
        return $json;*/
    }

    public function apiTest(){
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://reqres.in',
        ]);

        $query['page'] = '2';
        $query['per_page'] = '3';

        $param['query'] = $query;
          
        // $response = $client->request('GET', '/api/users', [
        //     // 'query' => [
        //     //     'page' => '2',
        //     //     'per_page' => '2'
        //     // ]

        //     'query' => $query
        // ]);

        $response = $client->request('GET', '/api/users', $param);
        $code = $response->getStatusCode();
        $body = $response->getBody();
        //$arr_body = json_decode($body);
        //print_r($body);
        //print_r($arr_body);
        echo $code.' '.$body . '<br><br>';

        $input['name'] = 'Thananon';
        $input['job'] = 'Software Developer';

        echo json_encode($input).'<br>';

        $param1['json'] = $input;

        echo json_encode($param1).'<br>';

        $response1 = $client->request('POST', '/api/users', $param1);
        $body1 = $response1->getBody();
        $code1 = $response1->getStatusCode();
        echo $code1.' '.$body1.'<br><br>';

        $response2 = $client->request('POST', '/api/users', ['json' => ['name' => 'Thananon', 'job' => 'Software Developer']]);
        //$response2 = $client->request('POST', '/api/users', ['json' => $param1]);
        $body2 = $response2->getBody();
        $code2 = $response2->getStatusCode();
        echo $code2.' '.$body2;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['id' =>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
