<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class WebhookController extends Controller{
    public function handle(Request $request){
        //logger($request);
        // $bodyJson = array();

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://test-fm-wsgw.intra.ais',
            'verify' => false,
        ]);

        Log::channel('webhook')->info($request);
        Log::channel('webhook')->info(count($request['alerts']));

        for($i = 0; $i < count($request['alerts']); $i++){
            Log::channel('webhook')->info('Loop '.$i);
            $input['module'] = 'AlertOpenshift';
            $input['amoName'] = '';
            $input['alarmName'] = '';
            $input['description'] = '';
            $input['node'] = '';

            if(array_key_exists('node',$request['alerts'][$i]['labels']))
                $input['amoName'] = $request['alerts'][$i]['labels']['node'];
            else if(array_key_exists('namespace',$request['alerts'][$i]['labels']))
                $input['amoName'] = $request['alerts'][$i]['labels']['namespace'];

            if(array_key_exists('alertname',$request['alerts'][$i]['labels']))
                $input['alarmName'] = $request['alerts'][$i]['labels']['alertname'];
                
            if(array_key_exists('description',$request['alerts'][$i]['annotations']))
                $input['description'] = $request['alerts'][$i]['annotations']['description'];

            if(array_key_exists('pod',$request['alerts'][$i]['labels'])){
                $input['node'] = $request['alerts'][$i]['labels']['pod'];
                //$input['pod'] = $request['alerts'][$i]['labels']['pod'];
            }
                
            //if($input['node'] == 'Pod-') $input['node'] = '';

            $input['mcZone'] = '';
            $input['systemName'] = 'OCP';
            $input['emsName'] = 'viking904.odin.valhalla.intra.ais';
            $input['emsIp'] = '10.183.146.5';
            $input['siteCode'] = '804';

            $input['severity'] = '';
            if(array_key_exists('severity',$request['alerts'][$i]['labels'])){
                if($request['alerts'][$i]['status'] == 'resolved')
                    $input['severity'] = 'clear';
                else if($request['alerts'][$i]['labels']['severity'] == 'none')
                    $input['severity'] = 'minor';
                else if($request['alerts'][$i]['labels']['severity'] == 'info')
                    $input['severity'] = 'minor';
                else if($request['alerts'][$i]['labels']['severity'] == 'warning')
                    $input['severity'] = 'major';
                else $input['severity'] = 'critical';
            }
                //$input['severity'] = $request['alerts'][$i]['labels']['severity'];

            $input['region'] = 'BKK';
            
            $input['nodeIp'] = '';
            if(array_key_exists('instance',$request['alerts'][$i]['labels']))
                $input['nodeIp'] = $request['alerts'][$i]['labels']['instance'];
            //if($input['nodeIp'] == 'Pod-') $input['nodeIp'] = '';

            $input['networkType'] = '';

            // if(array_key_exists('status',$request['alerts'][$i]))
            //     $input['status'] = $request['alerts'][$i]['status'];
            // if(array_key_exists('container',$request['alerts'][$i]['labels']))
            //     $input['container'] = $request['alerts'][$i]['labels']['container'];
            // if(array_key_exists('daemonset',$request['alerts'][$i]['labels']))
            //     $input['daemonset'] = $request['alerts'][$i]['labels']['daemonset'];
            // if(array_key_exists('endpoint',$request['alerts'][$i]['labels']))
            //     $input['endpoint'] = $request['alerts'][$i]['labels']['endpoint'];
            // if(array_key_exists('job',$request['alerts'][$i]['labels']))
            //     $input['job'] = $request['alerts'][$i]['labels']['job'];
            // if(array_key_exists('namespace',$request['alerts'][$i]['labels']))
            //     $input['namespace'] = $request['alerts'][$i]['labels']['namespace'];
            // if(array_key_exists('prometheus',$request['alerts'][$i]['labels']))
            //     $input['prometheus'] = $request['alerts'][$i]['labels']['prometheus'];
            // if(array_key_exists('service',$request['alerts'][$i]['labels']))
            //     $input['service'] = $request['alerts'][$i]['labels']['service'];
            // if(array_key_exists('summary',$request['alerts'][$i]['annotations']))
            //     $input['summary'] = $request['alerts'][$i]['annotations']['summary'];
            // if(array_key_exists('startsAt',$request['alerts'][$i]))
            //     $input['startsAt'] = $request['alerts'][$i]['startsAt'];
            // if(array_key_exists('endsAt',$request['alerts'][$i]))
            //     $input['endsAt'] = $request['alerts'][$i]['endsAt'];

            //Log::channel('webhook')->info('Input Ready!');
            //Log::channel('webhook')->info(json_encode($input));
            Log::channel('webhook')->info($input);

            $param['json'] = $input;

            try{
                $response = $client->request('POST', '/FMGateway/rest/service/sendAlarm', $param);
                $code = $response->getStatusCode();
                $body = $response->getBody();

                Log::channel('webhook')->info($code);
                Log::channel('webhook')->info($body);
            }catch(Exception $e){
                Log::channel('webhook')->info($e);
            }
        }

        //Log::channel('webhook')->info($request['commonLabels']['severity']);
        //Log::channel('webhook')->info($request['status']);
        return response()->json([$request],200);
    }
}